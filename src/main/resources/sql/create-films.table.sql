CREATE TABLE films (
                       id int PRIMARY KEY,
                       content varchar(255),
                       year int,
                       producer varchar(255)
);