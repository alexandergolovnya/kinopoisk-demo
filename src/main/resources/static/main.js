const HttpClient = function() {
    this.request = function(url, requestType, body, callbackFunction) {
        const httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState == 4 && httpRequest.status == 200)
                callbackFunction(httpRequest.responseText);
        }

        httpRequest.open(requestType, url, true);
        httpRequest.send(body);
    }
}

let filmsList = [];

const client = new HttpClient();
client.request('http://localhost:8090/films', "GET", null,function(response) {
    // do something with response
    filmsList = response;
    console.log("Films list: " + filmsList);
    const filmsContent = document.getElementById("filmsContent");
    filmsContent.innerHTML = filmsList;
});