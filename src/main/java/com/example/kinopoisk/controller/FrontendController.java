package com.example.kinopoisk.controller;

import com.example.kinopoisk.service.FilmsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class FrontendController {
    
    private final FilmsService filmsService;
    
    @GetMapping("")
    public String mainPage(Model model,
                           @RequestParam(value = "name", defaultValue = "World") String name) {
        model.addAttribute("name", name);
        model.addAttribute("films", filmsService.getAll());
        return "index";
    }
    
    @GetMapping("/test")
    public String startPageJS() {
        return "test";
    }
}
