package com.example.kinopoisk.controller;

import com.example.kinopoisk.model.Film;
import com.example.kinopoisk.service.FilmsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class FilmsController {
    
    private final FilmsService filmsService;
    
    @GetMapping("/films")
    public Iterable<Film> getAllElementsFromDatabase() {
        return filmsService.getAll();
    }
    
    @GetMapping("/films/{id}")
    public Film getElementFromDatabaseById(@PathVariable(value = "id") Integer id) {
        return filmsService.getById(id);
    }
    
    @PostMapping("/films")
    public Film createFilm(@RequestBody Film film) {
        return filmsService.create(film);
    }
    
    @PutMapping("/films/{id}")
    public Film updateFilm(@PathVariable(value = "id") Integer id, @RequestBody Film film) {
        return filmsService.update(id, film);
    }
    
    @DeleteMapping("/films/{id}")
    public void deleteFilm(@PathVariable(value = "id") Integer id) {
        filmsService.delete(id);
    }
}
