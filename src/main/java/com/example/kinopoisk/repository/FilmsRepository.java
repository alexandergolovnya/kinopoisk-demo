package com.example.kinopoisk.repository;

import com.example.kinopoisk.model.Film;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmsRepository extends CrudRepository<Film, Integer> {
}
