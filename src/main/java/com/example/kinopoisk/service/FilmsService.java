package com.example.kinopoisk.service;

import com.example.kinopoisk.model.Film;

import java.util.List;

public interface FilmsService {
    Film getById(Integer id);
    List<Film> getAll();
    Film create(Film film);
    Film update(Integer id, Film film);
    void delete(Integer id);
}
