package com.example.kinopoisk.service;

import com.example.kinopoisk.model.Film;
import com.example.kinopoisk.repository.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
@RequiredArgsConstructor
public class FilmsDatabaseService implements FilmsService {
    
    private final FilmsRepository filmsRepository;
    
    @PostConstruct
    public void init() {
        for (int i = 1; i < 10; i++) {
            Film film = new Film(generateId(), "Star Wars " + i, 1990 + i, "Ivan Ivanovich");
            filmsRepository.save(film);
        }
    }
    
    @Override
    public Film getById(Integer id) {
        return filmsRepository.findById(id)
                .orElseThrow(() -> getFilmNotFoundException(id));
    }
    
    @Override
    public List<Film> getAll() {
        Iterable<Film> filmsIterable = filmsRepository.findAll();
        List<Film> filmList = new ArrayList<>();
        filmsIterable.forEach(filmList::add);
        return filmList;
    }
    
    @Override
    public Film create(Film film) {
        film.setId(generateId());
        return filmsRepository.save(film);
    }
    
    @Override
    public Film update(Integer id, Film film) {
        // TODO: 29.11.2021 Add custom invalid response object with support for exception messages
        Film filmFromDatabase = filmsRepository.findById(id)
                .orElseThrow(() -> getFilmNotFoundException(id));
    
        filmFromDatabase.setContent(film.getContent());
        filmFromDatabase.setProducer(film.getProducer());
        filmFromDatabase.setYear(film.getYear());
        return filmsRepository.save(filmFromDatabase);
    }
    
    @Override
    public void delete(Integer id) {
        filmsRepository.deleteById(id);
    }
    
    private ResponseStatusException getFilmNotFoundException(Integer id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Film with id " + id + " doesn't exist");
    }
    
    private int generateId() {
        return ThreadLocalRandom.current().nextInt(0, 10000);
    }
}
