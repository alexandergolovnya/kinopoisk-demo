package com.example.kinopoisk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinopoiskDemoApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(KinopoiskDemoApplication.class, args);
    }
    
}
